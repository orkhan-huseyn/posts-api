function add(x, y) {
  if (typeof x !== "number" || typeof y !== "number") {
    throw new Error("Inputs must be numbers!");
  }

  return x + y;
}

module.exports = add;
