require("dotenv").config();

const express = require("express");
const cors = require("cors");
const path = require("path");
const authMiddleware = require("./middelware/auth");
const postRoutes = require("./app/posts");
const userRoutes = require("./app/users");
const chatRoutes = require("./app/messages");

const app = express();

app.use(cors());
app.use("/public", express.static(path.resolve(__dirname, "public")));
app.use(authMiddleware);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(postRoutes);
app.use(userRoutes);
app.use(chatRoutes);

module.exports = app;
