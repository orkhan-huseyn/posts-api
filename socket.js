const io = require("socket.io");
const jwt = require("jsonwebtoken");
const client = require("./database/database");

module.exports = (server) => {
  const ws = new io.Server(server, { cors: "*" });

  ws.on("connection", (socket) => {
    const accessToken = socket.handshake.query.accessToken;
    let user;

    jwt.verify(accessToken, process.env.SECRET_KEY, (err, decoded) => {
      if (err) {
        socket.disconnect();
      } else {
        user = decoded;
        socket.join(user.id);
      }
    });

    socket.on("start chat", (data) => {
      socket.join(data.userId);
    });

    socket.on("chat message", async (data) => {
      const fromUserId = user.id;
      const toUserId = data.toUserId;
      const content = data.content;

      await client.query(
        `INSERT INTO messages (content, from_user_id, to_user_id)
             VALUES ($1, $2, $3)`,
        [content, fromUserId, toUserId]
      );

      socket
        .to(toUserId)
        .emit("new message", { content: data.content, toUserId });
    });
  });
};
