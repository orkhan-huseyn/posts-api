const jwt = require("jsonwebtoken");


function authMiddleware(req, res, next) {
  if (
    req.url === "/registration" ||
    req.url === "/login" ||
    req.url.startsWith("/public")
  ) {
    next();
    return;
  }

  const accessToken = req.headers["authorization"];
  if (accessToken) {
    jwt.verify(accessToken, process.env.SECRET_KEY, (err, decoded) => {
      if (err) {
        res.status(401).send({
          message: "Invalid token",
        });
      } else {
        req.user = decoded;
        next();
      }
    });
  } else {
    res.status(401).send({
      message: "Unauthorized request",
    });
  }
}

module.exports = authMiddleware;
