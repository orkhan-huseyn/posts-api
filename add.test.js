const add = require("./add");

describe("add function", () => {
  it("adds two positive numbers correctly", () => {
    expect(add(5, 5)).toBe(10);
  });

  it("throws an error with non-number input", () => {
    expect(() => {
      add("5", "5");
    }).toThrowError();
  });
});
