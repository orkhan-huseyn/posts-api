const http = require("http");
const app = require("./app");
const initializeSocket = require("./socket");

const PORT = process.env.PORT || 8080;

const server = http.createServer(app);

initializeSocket(server);

server.listen(PORT, () => {
  console.log(`Posts API is running on port ${PORT}`);
});
