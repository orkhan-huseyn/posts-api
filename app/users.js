const express = require("express");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const client = require("../database/database");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });
const router = express.Router();
const SALT = process.env.SALT;
const SECRET_KEY = process.env.SECRET_KEY;

router.post("/registration", upload.single("image"), async (req, res) => {
  const { username, password, fullName } = req.body;

  if (!username || !password || !fullName) {
    res.status(400).send({
      message: "Username, password and fullName cannot be empty",
    });
    return;
  }

  const hashedPassword = crypto
    .pbkdf2Sync(password, SALT, 100000, 64, "sha512")
    .toString("hex");

  const existingUser = await client.query(
    `SELECT * FROM users WHERE username=$1`,
    [username]
  );

  if (existingUser.rowCount > 0) {
    res.status(400).send({
      message: "Username already exists",
    });
    return;
  }

  const imageUrl = "public/" + req.file.originalname;
  await client.query(
    `INSERT INTO users (username, password, full_name, image) 
     VALUES ($1, $2, $3, $4)`,
    [username, hashedPassword, fullName, imageUrl]
  );

  res.status(201).send();
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    res.status(400).send({
      message: "Username and password cannot be empty",
    });
    return;
  }

  const hashedPassword = crypto
    .pbkdf2Sync(password, SALT, 100000, 64, "sha512")
    .toString("hex");

  const result = await client.query(
    `SELECT * FROM users WHERE username=$1 AND password=$2`,
    [username, hashedPassword]
  );

  if (result.rowCount > 0) {
    const { password, ...theRest } = result.rows[0];
    const accessToken = jwt.sign(theRest, SECRET_KEY);

    res.status(200).send({
      accessToken,
    });
  } else {
    res.status(401).send({
      message: "Username or password is wrong",
    });
  }
});

module.exports = router;
