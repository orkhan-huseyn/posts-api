const express = require('express');
const client = require('../database/database');
const router = express.Router();

router.get('/posts', async (req, res) => {
  const { skip = 0, limit = 5 } = req.query;
  const result = await client.query(
    `
    SELECT
      posts.id,
      title,
      body,
      tags,
      (
        SELECT
          COUNT(*)
        FROM
          reactions
        WHERE
          post_id = posts.id
      ) as reactions,
      (
        SELECT
          COUNT(*)
        FROM
          comments
        WHERE
          post_id = posts.id
      ) as comments,
      posts.created_at,
      users.username
    FROM
      posts JOIN users ON users.id=posts.user_id ORDER BY posts.created_at DESC LIMIT $1 OFFSET $2;`,
    [limit, skip]
  );

  const posts = result.rows;
  for (let post of posts) {
    if (post.tags) post.tags = post.tags.split(',');
    else post.tags = [];
  }
  const countResult = await client.query('SELECT COUNT(*) FROM posts;');
  const total = Number(countResult.rows[0].count);

  res.status(200).send({
    posts,
    total,
  });
});

router.get('/posts/:id/comments', async (req, res) => {
  const result = await client.query(
    `
  SELECT
    comments.id as id,
    body,
    users.username as username
  FROM
    comments
    JOIN users ON users.id = comments.user_id 
    WHERE post_id=$1`,
    [req.params.id]
  );
  res.status(200).send({ comments: result.rows });
});

router.post('/posts', async (req, res) => {
  const { title, content, tags = [] } = req.body;

  if (!title || !content) {
    res.status(400).send({
      message: 'Title and content cannot be empty',
    });
    return;
  }

  await client.query(
    `INSERT INTO posts (title, body, tags, user_id) 
     VALUES ($1, $2, $3, $4)`,
    [title, content, tags.join(','), req.user.id]
  );
  res.status(201).send();
});

router.get('/posts/:id', async (req, res) => {
  const result = await client.query(
    `
  SELECT
    id,
    title,
    body,
    tags,
    (
      SELECT
        COUNT(*)
      FROM
        reactions
      WHERE
        post_id = id
    ) as reactions, 
    created_at 
    FROM posts WHERE id=$1`,
    [req.params.id]
  );
  if (result.rowCount > 0) {
    const post = result.rows[0];
    if (post.tags) post.tags = post.tags.split(',');
    else post.tags = [];
    res.status(200).send(result.rows[0]);
  } else {
    res.status(404).send({
      message: 'Post not found',
    });
  }
});

router.put('/posts/:id', async (req, res) => {
  const result = await client.query('SELECT * FROM posts WHERE id=$1', [
    req.params.id,
  ]);
  if (result.rowCount > 0) {
    const post = result.rows[0];
    if (post.user_id === req.user.id) {
      await client.query(`UPDATE posts SET title=$1, body=$2, WHERE id=$3`, [
        req.body.title,
        req.body.content,
        req.params.id,
      ]);
      res.status(200).send();
    } else {
      res.send(403).send({
        message: "You cannot update other people's posts.",
      });
    }
  } else {
    res.status(404).send({
      message: 'Post not found',
    });
  }
});

router.post('/posts/:id/comments', async (req, res) => {
  await client.query(
    `INSERT INTO comments (body, user_id, post_id) 
     VALUES ($1, $2, $3)`,
    [req.body.content, req.user.id, req.params.id]
  );
  res.status(201).send();
});

router.post('/posts/:id/reactions', async (req, res) => {
  const reactions = await client.query(
    `SELECT * FROM reactions WHERE user_id=$1 AND post_id=$2`,
    [req.user.id, req.params.id]
  );

  if (reactions.rowCount > 0) {
    const [reaction] = reactions.rows;
    await client.query('DELETE FROM reactions WHERE id=$1', [reaction.id]);
    res.status(200).send();
  } else {
    await client.query(
      `INSERT INTO reactions (user_id, post_id) 
       VALUES ($1, $2)`,
      [req.user.id, req.params.id]
    );
    res.status(201).send();
  }
});

router.get('/posts/:id/reactions', async (req, res) => {
  const reactions = await client.query(
    `SELECT * FROM reactions WHERE post_id=$1`,
    [req.params.id]
  );
  res.status(200).send(reactions.rows);
});

router.delete('/posts/:id', async (req, res) => {
  const result = await client.query('SELECT * FROM posts WHERE id=$1', [
    req.params.id,
  ]);

  if (result.rowCount > 0) {
    const post = result.rows[0];
    if (post.user_id === req.user.id) {
      await client.query('DELETE FROM posts WHERE id=$1', [req.params.id]);
      res.status(204).send();
    } else {
      res.status(403).send({
        message: "You cannot delete other people's posts.",
      });
    }
  } else {
    res.status(404).send({
      message: 'Post not found',
    });
  }
});

module.exports = router;
