const express = require("express");
const client = require("../database/database");

const router = express.Router();

router.get("/users", async (req, res) => {
  const result = await client.query(
    `SELECT id, username, full_name, image, (
        SELECT COUNT(*) FROM messages WHERE from_user_id=users.id AND to_user_id=$1 AND seen = FALSE
    ) as unread_messages FROM users WHERE id <> $1`,
    [req.user.id]
  );
  res.status(200).send(result.rows);
});

router.get("/users/:userId/messages", async (req, res) => {
  const result = await client.query(
    `SELECT id, content, (
        CASE 
            WHEN from_user_id=$1 THEN TRUE
            ELSE FALSE
        END
    ) is_from_self, seen, created_at FROM messages
    WHERE (from_user_id=$1 AND to_user_id=$2) OR (from_user_id=$2 AND to_user_id=$1)
     ORDER BY created_at`,
    [req.user.id, req.params.userId]
  );

  res.status(200).send(result.rows);
});

router.post("/users/:userId/messages", async (req, res) => {
  const { content } = req.body;

  if (!content) {
    res.status(400).send({
      message: "Message cannot be empty.",
    });
    return;
  }

  await client.query(
    `INSERT INTO messages (content, from_user_id, to_user_id)
         VALUES ($1, $2, $3)`,
    [content, req.user.id, req.params.userId]
  );

  res.status(201).send();
});

router.put("/messages/:messageId/seen", async (req, res) => {
  await client.query("UPDATE messages SET seen=TRUE WHERE id=$1", [
    req.params.messageId,
  ]);
  res.status(200).send();
});

router.put("/users/:userId/messages/seen", async (req, res) => {
  await client.query(
    `UPDATE messages SET seen=TRUE WHERE from_user_id=$1 AND to_user_id=$2`,
    [req.params.userId, req.user.id]
  );
  res.status(200).send();
});

module.exports = router;
